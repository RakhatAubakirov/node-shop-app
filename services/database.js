const oracledb = require('oracledb');
const dbConfig = require('../config/database.js');
 
async function initialize() {
  const pool = await oracledb.createPool(dbConfig.hrPool);
}

async function connect(){
  let conn = await oracledb.getConnection();
  return conn;
}

async function close() {
    await oracledb.getPool().close();
}
   
async function simpleExecute(conn, statement, binds = [], opts = {}) {
  
    opts.outFormat = oracledb.OBJECT;
    opts.autoCommit = false;
  
    try {
  
      const result = await conn.execute(statement, binds, opts);
  
      return result;
    } catch (err) {
        throw err;
    }
}
  
module.exports = {initialize, connect, close, simpleExecute};