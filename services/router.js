const express = require('express');
const router = new express.Router();
const products = require('../controllers/products.js');
const categories = require('../controllers/categories.js');
const clients = require('../controllers/clients');
const attributes = require('../controllers/attributes');
const statuses = require('../controllers/statuses');
const shipments = require('../controllers/shipments');
const payments = require('../controllers/payments');
const orders = require('../controllers/orders');
const orderItems = require('../controllers/orderItems');
const shipmentItems = require('../controllers/shipmentItems');
 
router.route('/products/:id?')
  .get(products.get)
  .post(products.post)
  .put(products.put)
  .delete(products.del);

router.route('/products/category/:category_id')
  .get(products.getByCategory);


router.route('/categories/:id?')
  .get(categories.get)
  .post(categories.post)
  .put(categories.put)
  .delete(categories.del);

router.route('/categories/:id/:products')
  .get(categories.get);

router.route('/clients/:id?')
  .get(clients.get)
  .post(clients.post)
  .put(clients.put)
  .delete(clients.del);

router.route('/attributes/:id?')
  .get(attributes.get)
  .post(attributes.post)
  .put(attributes.put)
  .delete(attributes.del);

router.route('/statuses/:id?')
  .get(statuses.get)
  .post(statuses.post)
  .put(statuses.put)
  .delete(statuses.del);

router.route('/shipments/:id?')
  .get(shipments.get)
  .post(shipments.post)
  .put(shipments.put)
  .delete(shipments.del);

router.route('/payments/:id?')
  .get(payments.get)
  .post(payments.post)
  .put(payments.put)
  .delete(payments.del);

router.route('/orders/:id?')
  .get(orders.get)
  .post(orders.post)
  .put(orders.put)
  .delete(orders.del);

router.route('/order-items/:id?')
  .get(orderItems.get)
  .post(orderItems.post)
  .put(orderItems.put)
  .delete(orderItems.del);  

router.route('/shipment-items/:id?')
  .get(shipmentItems.get)
  .post(shipmentItems.post)
  .put(shipmentItems.put)
  .delete(shipmentItems.del);    
 
module.exports = router;