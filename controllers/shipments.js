const shipments = require('../db_apis/shipments.js');
const database = require('../services/database')
 
async function get(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await shipments.find(conn, context);

    // await conn.close();

    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
    
  } catch (err) {
      res.status(500).json(err);
  } finally{
      await conn.close();
  }
}

function getProductFromRec(req) {
  const shipment = {
    address: req.body.address,
    wherefrom: req.body.wherefrom,
    sent_date: req.body.sent_date,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
  };
 
  return shipment;
}

function getProductFromRecPut(req) {
    const shipment = {
      address: req.body.address,
      wherefrom: req.body.wherefrom,
      send_date: req.body.send_date,
      updated_user: req.body.updated_user
    };
   
    return shipment;
  }
 
async function post(req, res, next) {
  let conn;
  try {
    let shipment = getProductFromRec(req);

    conn = await database.connect();
 
    shipment = await shipments.create(conn, shipment);

    await conn.commit();
 
    res.status(201).json(shipment);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let shipment = getProductFromRecPut(req);
 
    shipment.id = parseInt(req.params.id, 10);
 
    shipment = await shipments.update(conn, shipment);
 
    if (shipment !== null) {
        await conn.commit()
        return res.status(200).json(shipment);
    } 
    res.status(404).end();
    
  } catch (err) {
      await conn.rollback()
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();
    const id = parseInt(req.params.id, 10);
 
    const success = await shipments.delete(conn, id);
 
    if (success) {
        await conn.close();  
        res.status(204).end();
    } else {
        await conn.close();
        res.status(404).end();
    }
  } catch (err) {
      res.status(500).json(err);
  }
}
 
module.exports = {get, post, put, del};
