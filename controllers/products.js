const products = require('../db_apis/products.js');
const database = require('../services/database')
 
async function get(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await products.find(conn, {...req.query, ...req.params});

    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
  } catch (err) {
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

async function getByCategory(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await products.findByCategory(conn, context);

    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
  } catch (err) {
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const product = {
    name: req.body.name,
    price: req.body.price,
    description: req.body.description,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
    category_id: req.body.category_id
  };
 
  return product;
}

function getProductFromRecPut(req) {
    const product = {
      name: req.body.name,
      price: req.body.price,
      description: req.body.description,
      updated_user: req.body.updated_user,
      category_id: req.body.category_id
    };
   
    return product;
  } 

async function post(req, res, next) {
  let conn;
  try {
    let product = getProductFromRec(req);

    conn = await database.connect();
 
    product = await products.create(conn, product);

    await conn.commit();
 
    res.status(201).json(product);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let product = getProductFromRecPut(req);
 
    product.id = parseInt(req.params.id, 10);
 
    product = await products.update(conn, product);
 
    if (product !== null) {
        await conn.commit();  
        res.status(200).json(product);
    } 
    res.status(404).end();
  
} catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally{
      await conn.close();
  }
}

async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await products.delete(conn, id);
 
    if (success) {
        await conn.commit();  
        res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, getByCategory, post, put, del};

