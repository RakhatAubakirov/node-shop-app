const statuses = require('../db_apis/statuses.js');
const database = require('../services/database')
 
async function get(req, res, next) {

  // res.status(200).json(res);
  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await statuses.find(conn, context);

    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const orderStatus = {
    name: req.body.name,
    description: req.body.description,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
  };
 
  return orderStatus;
}

function getProductFromRecPut(req) {
    const orderStatus = {
      name: req.body.name,
      description: req.body.description,
      updated_user: req.body.updated_user
    };
   
    return orderStatus;
  }
 
async function post(req, res, next) {
  let conn;
  try {
    let orderStatus = getProductFromRec(req);

    let conn = await database.connect();
 
    orderStatus = await statuses.create(conn, orderStatus);

    await conn.commit();
 
    res.status(201).json(orderStatus);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let orderStatus = getProductFromRecPut(req);
 
    orderStatus.id = parseInt(req.params.id, 10);
 
    orderStatus = await statuses.update(conn, orderStatus);
 
    if (orderStatus !== null) {
        await conn.commit()
        return res.status(200).json(orderStatus);
    } 
    res.status(404).end();
    
  } catch (err) {
      await conn.rollback()
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await statuses.delete(id);
 
    if (success) {
        await conn.commit();  
        res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};

