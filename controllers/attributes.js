const attributes = require('../db_apis/attributes.js');
const database = require('../services/database')
 
async function get(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await attributes.find(conn, context);

    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).json(rows[0]);
      } else {
          res.status(404).end();
      }
    } else {
        res.status(200).json(rows);
    }
  } catch (err) {
      res.status(500).json(err);
  } finally{
      await conn.close();
  }
}

function getProductFromRec(req) {
  const attribute = {
    color: req.body.color,
    weight: req.body.weight,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
    product_id: req.body.product_id
  };
 
  return attribute;
}

function getProductFromRecPut(req) {
  const attribute = {
    color: req.body.color,
    weight: req.body.weight,
    updated_user: req.body.updated_user,
    product_id: req.body.product_id
  };
 
  return attribute;
}
 
async function post(req, res, next) {
  let conn;
  try {
    let attribute = getProductFromRec(req);

    conn = await database.connect();
 
    attribute = await attributes.create(conn, attribute);

    await conn.commit();

    res.status(201).json(attribute);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
    await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;

  try {
    
    conn = await database.connect();

    let attribute = getProductFromRecPut(req);
 
    attribute.id = parseInt(req.params.id, 10);
 
    attribute = await attributes.update(conn, attribute);
 
    if (attribute !== null) {
        await conn.commit();  
        return res.status(200).json(attribute);
    } 
    res.status(404).end();
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally{
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await attributes.delete(conn, id);
 
    if (success) {
        await conn.commit();  
        res.status(204).end();
    }
    res.status(404).end();

  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
    await conn.close();
  }
}
 
module.exports = {get, post, put, del};
