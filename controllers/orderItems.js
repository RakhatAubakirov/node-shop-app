const orderItems = require('../db_apis/orderItems.js');
const database = require('../services/database');
 
async function get(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await orderItems.find(conn, context);


    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
    
  } catch (err) {
      res.status(500).json(err);  
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const orderItem = {
    quantity: req.body.quantity,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
    product_id: req.body.product_id,
    order_id: req.body.order_id
  };
 
  return orderItem;
}

function getProductFromRecPut(req) {
    const orderItem = {
      quantity: req.body.quantity,
      updated_user: req.body.updated_user
    };
   
    return orderItem;
  }
 
async function post(req, res, next) {
  let conn;
  try {
    let orderItem = getProductFromRec(req);

    conn = await database.connect();
 
    orderItem = await orderItems.create(conn, orderItem);

    await conn.commit();
 
    res.status(201).json(orderItem);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let orderItem = getProductFromRecPut(req);
 
    orderItem.id = parseInt(req.params.id, 10);
 
    orderItem = await orderItems.update(conn, orderItem);
 
    if (orderItem !== null) {
        await conn.commit()
        return res.status(200).json(orderItem);
    } 
    res.status(404).end();
    
  } catch (err) {
      await conn.rollback()
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await orderItems.delete(conn, id);

 
    if (success) {
        await conn.commit();  
        res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};
