const orders = require('../db_apis/orders.js');
const database = require('../services/database');
 
async function get(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await orders.find(conn, {...req.query, ...req.oarams});

    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
  } catch (err) {
      res.status(500).json(err);
  }
}

function getProductFromRec(req) {
  const order = {
    amount: req.body.amount,
    address: req.body.address,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
    status_id: req.body.status_id,
    shipment_id: req.body.shipment_id,
    payment_id: req.body.payment_id,
    client_id: req.body.client_id
  };
 
  return order;
}

function getProductFromRecPut(req) {
    const order = {
      amount: req.body.amount,
      address: req.body.address,
      updated_user: req.body.updated_user
    };
   
    return order;
  }
 
async function post(req, res, next) {
  let conn;
  try {
    let order = getProductFromRec(req);

    conn = await database.connect();
 
    order = await orders.create(conn, order);

    await conn.commit();
 
    res.status(201).json(order);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let order = getProductFromRecPut(req);
 
    order.id = parseInt(req.params.id, 10);
 
    order = await orders.update(conn, order);
 
    if (order !== null) {
        await conn.commit()
        return res.status(200).json(order);
    } 
    res.status(404).end();
    
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await orders.delete(conn, id);
 
    if (success) {
        await conn.commit();
        res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};

