const database = require('../services/database.js');
const clients = require('../db_apis/clients.js');
 
async function get(req, res, next) {
  let conn;
  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await clients.find(conn, context);
    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
    
  } catch (err) {
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const client = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    age: req.body.age,
    city: req.body.city,
    created_user: req.body.created_user,
    updated_user: req.body.created_user
  };
 
  return client;
}

function getProductFromRecPut(req) {
  const client = {
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    age: req.body.age,
    city: req.body.city,
    updated_user: req.body.updated_user
  };
 
  return client;
}
 
async function post(req, res, next) {
  let conn;
  try {
    let client = getProductFromRec(req);
 
    conn = await database.connect();

    client = await clients.create(conn, client);
 
    await conn.commit();
    res.status(201).json(client);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    conn = await database.connect();
    let client = getProductFromRecPut(req);
 
    client.id = parseInt(req.params.id, 10);
 
    client = await clients.update(conn, client);
 
    if (client !== null) {
      await conn.commit();
      res.status(200).json(client);
    } else {
      res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    const id = parseInt(req.params.id, 10);
 
    const success = await clients.delete(id);
 
    if (success) {
      await conn.commit();
      res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};
