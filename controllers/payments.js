const payments = require('../db_apis/payments.js');
const database = require('../services/database')
 
async function get(req, res, next) {

  let conn;
  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await payments.find(conn, context);


    if (!rows.length) {
      return res.status(404).end();
    } 
    
    if (req.params.id) {
      return res.status(200).json(rows[0]);
    }
    
    res.status(200).json(rows);
  } catch (err) {
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const payment = {
    total: req.body.total,
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
  };
 
  return payment;
}

function getProductFromRecPut(req) {
    const payment = {
      total: req.body.total,
      updated_user: req.body.updated_user
    };
   
    return payment;
  }
 
async function post(req, res, next) {
  let conn;
  try {
    let payment = getProductFromRec(req);

    conn = await database.connect();
 
    payment = await payments.create(conn, payment);

    await conn.commit();
 
    res.status(201).json(payment);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let payment = getProductFromRecPut(req);
 
    payment.id = parseInt(req.params.id, 10);
 
    payment = await payments.update(conn, payment);
 
    if (payment !== null) {
        await conn.commit()
        return res.status(200).json(payment);
    } 
    res.status(404).end();
    
  } catch (err) {
      await conn.rollback()
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await payments.delete(conn, id);
 
    if (success) {
        await conn.commit();  
        res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};

