const categories = require('../db_apis/categories.js');
const database = require('../services/database')

async function get(req, res, next) {
  let conn;
  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await categories.find(conn, {...req.query, ...req.params});
 
    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).json(rows[0]);
      } 
      res.status(404).end();
    } else {
        res.status(200).json(rows);
    }
  } catch (err) {
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const category = {
    name: req.body.name,
    description: req.body.description,
    created_user: req.body.created_user,
    updated_user: req.body.created_user
  };
 
  return category;
}

function getProductFromRecPut(req) {
  const category = {
    name: req.body.name,
    description: req.body.description,
    updated_user: req.body.updated_user
  };
 
  return category;
}
 
async function post(req, res, next) {
  let conn;

  try {
    let category = getProductFromRec(req);

    conn = await database.connect();
   
    category = await categories.create(conn, category);
 
    await conn.commit();
    res.status(201).json(category);
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  }
}
 
async function put(req, res, next) {
  let conn; 
  try {
    conn = await database.connect();  
    let category = getProductFromRecPut(req);
 
    category.id = parseInt(req.params.id, 10);
 
    category = await categories.update(conn, category);
 
    if (category !== null) {
        await conn.commit();
        res.status(200).json(category);
    }
    res.status(404).end();
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;

  try {
    conn = await database.connect();
  
    const id = parseInt(req.params.id, 10);
 
    const success = await categories.delete(conn, id);
 
    if (success) {
        await conn.commit();
        res.status(204).end();
    }
    res.status(404).end();
  } catch (err) {
      await conn.rollback();
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};
