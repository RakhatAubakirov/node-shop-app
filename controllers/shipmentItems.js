const shipmentItems = require('../db_apis/shipmentItems.js');
const database = require('../services/database');
 
async function get(req, res, next) {

  let conn;

  try {
    const context = {};

    conn = await database.connect();
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await shipmentItems.find(conn, context);

    // await conn.close();

    if (!rows.length) {
        return res.status(404).end();
      } 
      
      if (req.params.id) {
        return res.status(200).json(rows[0]);
      }
      
      res.status(200).json(rows);
  } catch (err) {
        res.status(500).json(err);
  } finally {
      await conn.close();
  }
}

function getProductFromRec(req) {
  const orderItem = {
    created_user: req.body.created_user,
    updated_user: req.body.created_user,
    shipment_id: req.body.product_id,
    order_id: req.body.order_id
  };
 
  return orderItem;
}

function getProductFromRecPut(req) {
    const orderItem = {
      updated_user: req.body.updated_user
    };
   
    return orderItem;
  }
 
async function post(req, res, next) {
  let conn;  
  try {
    let shipmentItem = getProductFromRec(req);

    conn = await database.connect();
 
    shipmentItem = await shipmentItems.create(conn, shipmentItem);

    await conn.commit();
 
    res.status(201).json(shipmentItem);
  } catch (err) {
      await commit.rollback();
      res.status(500).json(err);
  }
}
 
async function put(req, res, next) {
  let conn;
  try {
    
    conn = await database.connect();

    let shipmentItem = getProductFromRecPut(req);
 
    shipmentItem.id = parseInt(req.params.id, 10);
 
    shipmentItem = await shipmentItems.update(conn, shipmentItem);
    // orderStatus.sassd = 343
    // orderStatus = await statuses.update(conn, orderStatus)
 
    if (shipmentItem !== null) {
        await conn.commit()
        return res.status(200).json(shipmentItem);
    } 
    res.status(404).end();
    
  } catch (err) {
      await conn.rollback()
      res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
async function del(req, res, next) {
  let conn;
  try {
    conn = await database.connect();

    const id = parseInt(req.params.id, 10);
 
    const success = await shipmentItems.delete(conn, id);
 
    if (success) {
        await conn.commit();  
        res.status(204).end();
    } else {
        res.status(404).end();
    }
  } catch (err) {
     await conn.rollback(); 
     res.status(500).json(err);
  } finally {
      await conn.close();
  }
}
 
module.exports = {get, post, put, del};
