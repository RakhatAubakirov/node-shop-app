const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    total "total",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user"
  from payment_detail`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql = 
 `insert into payment_detail (
    id,
    total,
    created_date,
    updated_date,
    created_user,
    updated_user
  ) values (
    payments_seq.nextval,
    :total,
    sysdate,
    sysdate,
    :created_user,
    :updated_user
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const payment = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, payment);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update payment_detail
  set total = :total,
    updated_date = sysdate,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const client = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, client);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return client;
  }
    return null;
  
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
 
 delete from payment_detail
 where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;