const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    color "color",
    weight "weight",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user"
  from attribute`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql =
 `insert into attribute (
    id,
    color,
    weight,
    created_date,
    updated_date,
    created_user,
    updated_user,
    product_id
  ) values (
    attribute_seq.nextval,
    :color,
    :weight,
    sysdate,
    sysdate,
    :created_user,
    :updated_user,
    :product_id
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const attribute = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, attribute);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update attribute
  set color = :color,
    weight = :weight,
    updated_date = sysdate,
    updated_user = :updated_user,
    product_id = :product_id
  where id = :id`;
 
async function update(conn, emp) {
  const attribute = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, attribute);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return attribute;
  } else {
    return null;
  }
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
    delete from attribute
    where id = :id;
 
    delete from product
    where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;