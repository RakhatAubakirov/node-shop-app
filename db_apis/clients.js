const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    first_name "first_name",
    last_name "last_name",
    age "age",
    city "city",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user"
  from client`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql = 
 `insert into client (
    id,
    first_name,
    last_name,
    age,
    city,
    created_date,
    updated_date,
    created_user,
    updated_user
  ) values (
    client_seq.nextval,
    :first_name,
    :last_name,
    :age,
    :city,
    sysdate,
    sysdate,
    :created_user,
    :updated_user
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const client = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, client);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update client
  set first_name = :first_name,
    last_name = :last_name,
    age = :age,
    city = :city,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const client = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, client);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return client;
  }
    return null;
  
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
 
 delete from client
 where id = :id;
 
 delete from order_detail
 where client_id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;