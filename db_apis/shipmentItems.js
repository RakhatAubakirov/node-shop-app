const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user",
    shipment_id "product_id",
    order_id "order_id"
  from shipment_item`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql = 
 `insert into shipment_item (
    id,
    created_date,
    updated_date,
    created_user,
    updated_user,
    shipment_id,
    order_id
  ) values (
    shipment_item_seq.nextval,
    sysdate,
    sysdate,
    :created_user,
    :updated_user,
    :shipment_id,
    :order_id
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const shipmentItem = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, shipmentItem);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update shipment_item
  set updated_date = sysdate,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const shipmentItem = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, shipmentItem);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return shipmentItem;
  }
    return null;
  
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
 
 delete from shipment_item
 where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;