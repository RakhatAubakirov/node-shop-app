const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    name "name",
    description "description",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user"
  from category`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const getProductsQuery = 
  `select *
    from product as p inner join category c 
  on p.category_id = c.id`;
 
async function findProducts(conn, context) {
  let query = getProductsQuery;
  const binds = {};
 
  if (context.id) {
    binds.product_id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.findProducts = findProducts;

const createSql =
 `insert into category (
    id,
    name,
    description,
    created_date,
    updated_date,
    created_user,
    updated_user
  ) values (
    category_seq.nextval,
    :name,
    :description,
    sysdate,
    sysdate,
    :created_user,
    :updated_user
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const category = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, category);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update category
  set name = :name,
    description = :description,
    updated_date = sysdate,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const category = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, category);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return category;
  } else {
    return null;
  }
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
    delete from order_item
    where product_id = :id;

    delete from product
    where category_id = :id;
 
    delete from category
    where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;