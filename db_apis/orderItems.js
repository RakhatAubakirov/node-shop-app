const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
//  `select id "id",
//     quantity "quantity",
//     created_date "created_date",
//     updated_date "updated_date",
//     created_user "created_user",
//     updated_user "updated_user",
//     product_id "product_id",
//     order_id "order_id"
//   from order_item`;
`
      select 
        name, 
        price, 
        description, 
        quantity
      from product p, order_item o
      where p.id = o.product_id
    `;

 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }

  // if(context.productId) {
  //   binds.productid = context.productId;
  //   query = `
  //     select name, price, description, quantity
  //     from product p, order_item o
  //     where p.id = o.product_id and o.order_id =  and :productId = p.id
  //   `;
  // }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql = 
 `insert into order_item (
    id,
    quantity,
    created_date,
    updated_date,
    created_user,
    updated_user,
    product_id,
    order_id
  ) values (
    order_item_seq.nextval,
    :quantity,
    sysdate,
    sysdate,
    :created_user,
    :updated_user,
    :product_id,
    :order_id
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const orderItem = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, orderItem);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update order_item
  set quantity = :quantity,
    updated_date = sysdate,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const orderItem = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, orderItem);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return orderItem;
  }
    return null;
  
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
 
 delete from order_item
 where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;