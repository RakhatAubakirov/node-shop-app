const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    address "address",
    wherefrom "wherefrom",
    sent_date "sent_date",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user"
  from shipment`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql = 
 `insert into shipment (
    id,
    address,
    wherefrom,
    sent_date,
    created_date,
    updated_date,
    created_user,
    updated_user
  ) values (
    shipment_seq.nextval,
    :address,
    :wherefrom,
    :sent_date,
    sysdate,
    sysdate,
    :created_user,
    :updated_user
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const shipment = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       
        const result = await database.simpleExecute(conn, createSql, shipment);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update shipment
  set address = :address,
    wherefrom = :wherefrom,
    sent_date = :sent_date,
    updated_date = sysdate,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const shipment = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, shipment);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return shipment;
  }
    return null;
  
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
 
 delete from shipment
 where id = :id;

 delete from order_detail
 where shipment_id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;