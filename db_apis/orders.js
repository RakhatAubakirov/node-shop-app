const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    amount "amount",
    address "address",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user",
    status_id "status_id",
    shipment_id "shipment_id",
    payment_id "payment_id",
    client_id "client_id"
  from order_detail`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }

  if (context.statusId) {
    binds.statusid = context.statusId;
    query = `
      select id
      from order_detail
      where status_id = 4 and :statusid = 4
    `;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

const createSql = 
 `insert into order_detail (
    id,
    amount,
    address,
    created_date,
    updated_date,
    created_user,
    updated_user,
    status_id,
    shipment_id,
    payment_id,
    client_id

  ) values (
    order_detail_seq.nextval,
    :amount,
    :address,
    sysdate,
    sysdate,
    :created_user,
    :updated_user,
    :status_id,
    :shipment_id,
    :payment_id,
    :client_id
  ) returning id
  into :id`;
 
async function create(conn, emp) {
    try {
        const client = Object.assign({
          id : {
              dir: oracledb.BIND_OUT,
              type: oracledb.NUMBER
            }
        }, emp);
       console.log(createSql)
       console.log(client)
        const result = await database.simpleExecute(conn, createSql, client);
        
        return result.outBinds.id[0];
    } catch(e) {
        throw e
    }
}
 
module.exports.create = create;

const updateSql =
 `update order_detail
  set amount = :amount,
    address = :address,
    updated_date = sysdate,
    updated_user = :updated_user
  where id = :id`;
 
async function update(conn, emp) {
  const order = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, order);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return order;
  }
    return null;
  
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
 
 delete from order_detail
 where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;