const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select id "id",
    name "name",
    price "price",
    description "description",
    created_date "created_date",
    updated_date "updated_date",
    created_user "created_user",
    updated_user "updated_user",
    category_id "category_id"
  from product`;

const queryCategory = 
 `select *
    from product as p inner join category c 
    on p.category_id = c.id`;
 
async function find(conn, context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
    query += `\nwhere id = :id`;
  }

  if (context.categoryId) {
    binds.categoryid = context.categoryId;
    query = `
      select p.*
      from 
        product p, category c 
      where p.category_id = c.id and :categoryid = c.id
    `;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}
 
module.exports.find = find;

async function findByCategory(conn, context) {
  let query = queryCategory;
  const binds = {};
 
  if (context.id) {
    binds.category_id = context.id;
 
    query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(conn, query, binds);
 
  return result.rows;
}

module.exports.findByCategory = findByCategory;

const createSql = 
 `insert into product (
    id,
    name,
    price,
    description,
    created_date,
    updated_date,
    created_user,
    updated_user,
    category_id
  ) values (
    product_seq.nextval,
    :name,
    :price,
    :description,
    sysdate,
    sysdate,
    :created_user,
    :updated_user,
    :category_id
  ) returning id
  into :id`;
 
async function create(conn, emp) {
  try {
    const product = Object.assign({
      id : {
          dir: oracledb.BIND_OUT,
          type: oracledb.NUMBER
        }
    }, emp);
   
    const result = await database.simpleExecute(conn, createSql, product);
    
    return result.outBinds.id[0];
} catch(e) {
    throw e
}
}
 
module.exports.create = create;

const updateSql =
 `update product
  set name = :name,
    price = :price,
    description = :description,
    updated_date = sysdate,
    updated_user = :updated_user,
    category_id = :category_id
  where id = :id`;
 
async function update(conn, emp) {
  const product = Object.assign({}, emp);
  const result = await database.simpleExecute(conn, updateSql, product);
 
  if (result.rowsAffected && result.rowsAffected === 1) {
    return product;
  } else {
    return null;
  }
}
 
module.exports.update = update;

const deleteSql =
 `begin
 
    delete from product
    where id = :id;
 
    delete from product
    where id = :id;
 
    :rowcount := sql%rowcount;
 
  end;`
 
async function del(conn, id) {
  const binds = {
    id: id,
    rowcount: {
      dir: oracledb.BIND_OUT,
      type: oracledb.NUMBER
    }
  }
  const result = await database.simpleExecute(conn, deleteSql, binds);
 
  return result.outBinds.rowcount === 1;
}
 
module.exports.delete = del;
